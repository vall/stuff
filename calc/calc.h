#include <stdio.h>
#include <ctype.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>

struct Variable {
	char *name;
	double val;
	struct Variable *next;
};

struct Variable *top = NULL;

struct Variable *lookup(char *name)
{
	struct Variable *tmp;

	for (tmp = top; tmp != NULL; tmp = tmp->next)
		if (!strcmp(tmp->name, name))
			return tmp;
	return 0;
}

struct Variable *install(char *name, int val)
{
	struct Variable *tmp;

	tmp = (struct Variable *) malloc(sizeof(struct Variable));
	tmp->name = malloc(strlen(name) + 1);
	strcpy(tmp->name, name);
	tmp->val = val;
	tmp->next = top;
	top = tmp;

	return tmp;
}
