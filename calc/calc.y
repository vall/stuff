%{
#include "calc.h"
%}
%union {
	double val;
	struct Variable *var;
}
%token <val> NUMBER
%token <var> VAR
%type  <val> expr asgn
%right '='
%left '+' '-'
%left '*' '/' '%'
%left UNARYMINUS
%left UNARYPLUS
%%
list:
	| list '\n'
	| list asgn '\n'
	| list expr '\n' { printf("\t%.8g\n", $2); }
	| error '\n' { yyerrok; }
	;
asgn:	VAR '=' expr { $$ = $1->val = $3; }
	;
expr:	NUMBER { $$ = $1; }
	| VAR { $$ = $1->val; }
	| expr '+' expr { $$ = $1 + $3; }
	| expr '-' expr { $$ = $1 - $3; }
	| expr '*' expr { $$ = $1 * $3; }
	| expr '/' expr {
				if ($3 == 0) execerror("division by zero");
				$$ = $1 / $3;
			}
	| expr '%' expr {
				if ($3 == 0) execerror("division by zero");
				$$ = $1 - (int)($1 / $3) * $3;
			}
	| '(' expr ')' { $$ = $2; }
	| '-' expr %prec UNARYMINUS { $$ = -$2; }
	| '+' expr %prec UNARYPLUS { $$ = $2; }
	;
%%
jmp_buf begin;

int main()
{
	setjmp(begin);
	yyparse();
}

int yylex()
{
	int c;
	char *name, *tmp;
	struct Variable *v;

	while ((c = getchar()) == ' ' || c == '\t')
		;
	if (c == EOF)
		return 0;
	if (c == '.' || isdigit(c)) {
		ungetc(c, stdin);
		scanf("%lf", &yylval.val);
		return NUMBER;
	}
	name = tmp = malloc(100);
	if (islower(c)) {
		do {
			*tmp++ = c;
		} while (islower(c = getchar()) && c != EOF);
		ungetc(c, stdin);
		*tmp = '\0';
		if ((v = lookup(name)) == 0)
			v = install(name, 0.0);
		yylval.var = v;
		return VAR;
	}
	return c;
}

void yyerror(char *msg)
{
	fprintf(stderr, "calc: %s\n", msg);
}

void execerror(char *msg)
{
	fprintf(stderr, "calc: %s\n", msg);
	longjmp(begin, 0);
}
