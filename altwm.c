#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define SWKEY	XK_Tab
#define CLOSKEY XK_q
#define QUITKEY XK_Escape
#define LMODKEY	XK_Alt_L
#define RMODKEY	XK_Alt_R
#define MODMASK	Mod1Mask

int  getlist(Window **wlist, unsigned int *wnum);
void grabkeys(int count, ...);
void closewin(Window win);
int  errhandler(Display *disp, XErrorEvent *errevent);

static Display *disp;
static Window root;

int main(int argc, char *argv[])
{
	Window *wlist, tmpwin;
	XWindowAttributes rattr;
	XWindowChanges wconf;
	XEvent event;
	unsigned int wnum;
	int i;
	const int confmask = CWX | CWY | CWWidth | CWHeight | CWBorderWidth;

	if (!(disp = XOpenDisplay(NULL))) {
		fprintf(stderr, "altwm: error: can't open display. closing.\n");
		exit(1);
	}
	root = DefaultRootWindow(disp);
	XGetWindowAttributes(disp, root, &rattr);
	if (rattr.all_event_masks & SubstructureRedirectMask) {
		fprintf(stderr, "altwm: error: another window manager "
				"is already in use. closing.\n");
		XCloseDisplay(disp);
		exit(1);
	}

	XSetErrorHandler(&errhandler);
	XSelectInput(disp, root,
	    KeyPressMask | KeyReleaseMask | SubstructureNotifyMask);

	const KeyCode swkey   = XKeysymToKeycode(disp, SWKEY);
	const KeyCode closkey = XKeysymToKeycode(disp, CLOSKEY);
	const KeyCode quitkey = XKeysymToKeycode(disp, QUITKEY);
	const KeyCode lmodkey = XKeysymToKeycode(disp, LMODKEY);
	const KeyCode rmodkey = XKeysymToKeycode(disp, RMODKEY);
	grabkeys(3, swkey, closkey, quitkey);

	wconf.x = wconf.y = 0;
	wconf.border_width = 0;
	wconf.width = rattr.width;
	wconf.height = rattr.height;
	if (getlist(&wlist, &wnum)) {
		XSetInputFocus(disp, wlist[0], RevertToParent, CurrentTime);
		for (i = 0; i < wnum; i++)
			XConfigureWindow(disp, wlist[i], confmask, &wconf);
	}

	for (i = 1; ; ) {
		XNextEvent(disp, &event);
		switch (event.type) {
		case KeyPress:
			if (event.xkey.keycode == swkey && wnum > 1) {
				tmpwin = wlist[0];
				if (i < wnum) {
					wlist[0] = wlist[i];
					wlist[i++] = tmpwin;
				}
				else {
					for (i = 1; i < wnum; i++)
						wlist[i - 1] = wlist[i];
					wlist[i - 1] = tmpwin;
					i = 1;
				}
				XRestackWindows(disp, wlist, wnum);
				XSetInputFocus(disp, wlist[0], RevertToParent,
				    CurrentTime);
				XGrabKeyboard(disp, root, True, GrabModeAsync,
				    GrabModeAsync, CurrentTime);
			}
			else if (event.xkey.keycode == closkey && wnum > 0) {
				closewin(wlist[0]);
			}
			else if (event.xkey.keycode == quitkey) {
				XCloseDisplay(disp);
				return 0;
			}
			break;
		case KeyRelease:
			if (event.xkey.keycode == lmodkey ||
			    event.xkey.keycode == rmodkey) {
				XUngrabKeyboard(disp, CurrentTime);
				grabkeys(3, swkey, closkey, quitkey);
				i = 1;
			}
			break;
		case MapNotify:
			if (event.xmap.override_redirect == False)
				XConfigureWindow(disp, event.xmap.window,
				    confmask, &wconf);
		case UnmapNotify:
			if (getlist(&wlist, &wnum))
				XSetInputFocus(disp, wlist[0], RevertToParent,
				    CurrentTime);
		}
	}

	XCloseDisplay(disp);
	return 0;
}

int getlist(Window **wlist, unsigned int *wnum)
{
	Window tmpwin, tmpwin2;
	XWindowAttributes attr;
	unsigned int num;
	int i;

	XQueryTree(disp, root, &tmpwin, &tmpwin2, wlist, &num);
	for (i = 0, *wnum = 0; i < num; i++) {
		XGetWindowAttributes(disp, (*wlist)[i], &attr);
		if (attr.override_redirect == False &&
		    attr.map_state == IsViewable)
			(*wlist)[(*wnum)++] = (*wlist)[i];
	}
	if (*wnum > 1) {
		for (i = 0; i <= (*wnum / 2 - 1); i++) {
			tmpwin = (*wlist)[i];
			(*wlist)[i] = (*wlist)[*wnum - 1 - i];
			(*wlist)[*wnum - 1 - i] = tmpwin;
		}
	}
	return *wnum;
}

void grabkeys(int count, ...)
{
	va_list ap;
	KeyCode code;

	va_start(ap, count);
	while (count--) {
		code = va_arg(ap, int);
		XGrabKey(disp, code, MODMASK, root, False,
		    GrabModeAsync, GrabModeAsync);
		XGrabKey(disp, code, MODMASK | LockMask, root, False,
		    GrabModeAsync, GrabModeAsync);
		XGrabKey(disp, code, MODMASK | Mod2Mask, root, False,
		    GrabModeAsync, GrabModeAsync);
		XGrabKey(disp, code, MODMASK | LockMask | Mod2Mask, root, False,
		    GrabModeAsync, GrabModeAsync);
	}
	va_end(ap);
}

void closewin(Window win)
{
	XEvent *delevent;

	delevent = malloc(sizeof(XEvent));
	delevent->type = ClientMessage;
	delevent->xclient.display = disp;
	delevent->xclient.window = win;
	delevent->xclient.message_type = XInternAtom(disp, "WM_PROTOCOLS",True);
	delevent->xclient.format = 32;
	delevent->xclient.data.l[0] = XInternAtom(disp,"WM_DELETE_WINDOW",True);
	delevent->xclient.data.l[1] = CurrentTime;
	XSendEvent(disp, win, False, NoEventMask, delevent);
}

int  errhandler(Display *disp, XErrorEvent *errevent)
{
	char errname[BUFSIZ];

	if(errevent->error_code == BadAccess) {
		fprintf(stderr, "altwm: error: hotkeys are already grabbed "
				"by another client. closing.\n");
		exit(1);
	}
	else {
		XGetErrorText(disp, errevent->error_code, errname, BUFSIZ);
		fprintf(stderr, "altwm: warning: %s\n", errname);
	}
	return 0;
}
