#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

char *bitstostr(unsigned int bitnum);
unsigned int iptobits(char *ip);
unsigned int masktobits(char *mask);

unsigned int fault = 0;

int main()
{
	char *ip = malloc(32), *mask = malloc(32);
	unsigned int bitip, bitmask, bitnet;

	printf("IP: ");
	scanf("%s", ip);
	if ((bitip = iptobits(ip)) == 0 && fault) {
		fprintf(stderr, "mask: error: wrong IP\n");
		return -1;
	}

	printf("Mask: ");
	scanf("%s", mask);
	if ((bitmask = (masktobits(mask))) == 0 && fault) {
		fprintf(stderr, "mask: error: wrong mask\n");
		return -1;
	}

	bitnet = bitip & bitmask;
	printf("Net: %s\n", bitstostr(bitnet));
	printf("Hosts: %u\n", ~bitmask > 1 ? ~bitmask - 1 : 1);
	printf("Broadcast: %s\n", bitstostr(bitnet | ~bitmask));
}

char *bitstostr(unsigned int bitnum)
{
	unsigned int n;
	char *strmask, *bstrmask, *strbyte;

	bstrmask = strmask = malloc(16);
	strbyte = malloc(4);
	for (n = 4; n; n--) {
		sprintf(strbyte, "%u", 255 & (bitnum >> (8 * (n - 1))));
		strcat(strmask, strbyte);
		if (n > 1)
			strcat(strmask, ".");
	}
	strmask = '\0';
	return bstrmask;
}

unsigned int iptobits(char *ip)
{
	unsigned int byte, ipnum = 0, dots = 0;

	while (*ip != '\0') {
		for (byte = 0; isdigit(*ip); ip++)
			byte = byte * 10 + *ip - '0';
		if (byte > 255) {
			fault = 1;
			return 0;
		}
		if (*ip == '.')
			ip++, dots++;
		if (dots > 3) {
			fault = 1;
			return 0;
		}
		ipnum = (ipnum << 8) + byte;
	}
	return ipnum;
}

unsigned int masktobits(char *mask)
{
	unsigned int bitmask, nummask;

	if (strlen(mask) < 3 ) {
		for (nummask = 0; *mask != '\0'; mask++)
			nummask = nummask * 10 + *mask - '0';
		if (nummask > 32) {
			fault = 1;
			return 0;
		}
		bitmask = ~0;
		while (32 - nummask++)
			bitmask = bitmask << 1;
		return bitmask;
	}
	else
		nummask = iptobits(mask);

	return nummask;
}
