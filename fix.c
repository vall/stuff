#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

char *best(char *path, char *name);
int distan(char *name, char *guess);

int main(int argc, char *argv[])
{
	char *pold, new[MAXNAMLEN + 1], *pnew, part[BUFSIZ], *ppart, *pbest;

	if (argc > 1)
		pold = argv[1];
	else {
		fprintf(stderr, "usage: spname /path/to/file\n");
		return -1;
	}

	pnew = new;
	for ( ; ; ) {
		while (*pold == '/')
			*pnew++ = *pold++;
		*pnew = '\0';
		if (*pold == '\0') {
			printf ("%s\n", new);
			return 0;
		}
		ppart = part;
		while (*pold != '/' && *pold != '\0')
			*ppart++ = *pold++;
		*ppart = '\0';
		if ((pbest = best(new, part)) == NULL) {
			fprintf(stderr, "%s: wrong name\n", part);
			return -1;
		}
		while ((*pnew = *pbest++))
			pnew++;
	}
}

char *best(char *path, char *name)
{
	DIR *dir;
	struct dirent *fd;
	int bestdist = 100, dist;
	char *best = NULL;

	if (path[0] == '\0')
		strcpy(path, "./");
	if ((dir = opendir(path)) == NULL) {
		fprintf(stderr, "%s: %s\n", path, strerror(errno));
		return NULL;
	}
	while ((fd = readdir(dir)) != NULL) {
		dist = distan(name, fd->d_name);
		if (dist == 0)
			return name;
		if (dist < bestdist) {
			bestdist = dist;
			best = fd->d_name;
		}
	}
	return best;
}

int distan(char *name, char *guess)
{
	int i = 0, y = 0, dist = 0;

	while (i < MAXNAMLEN && y < MAXNAMLEN ) {
		while (name[i] == guess[y]) {
			if (name[i] == '\0')
				return dist;
			i++, y++;
		}
		if (name[i] == guess[y + 1])
			y++;
		else if (name[i + 1] == guess[y])
			i++;
		else
			i++, y++;
		dist++;
	}
	return dist;
}
