title="please don't fork me";
sidemenu='books links notes freebsd playlist';

# side menu is permanent for all pages
nav=$(echo "$sidemenu" | sed 's/ /\n/g' | sed '/^$/d' | \
                         sed 's:.*:<a href="/html/&.html">&</a>:; s/^/      /');

# template of all html pages
template='
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/style.css">
    <title></title>
  </head>
  <body>
    <nav>
      <a href="/">..</a>
      <a href="https://youtube.com/justsomelessons/videos" target="_blank">youtube</a>
      <a href="https://codeberg.org/vall" target="_blank">git</a>
    </nav>
    <main>
<pre>
</pre>
    </main>
  </body>
</html>';

# helper function
rmtail() {
  echo $1 | sed 's/.txt//';
}

cd $(dirname "$0");

# main function
makehtml() {
  if [ $1 = index ]
  then
    dir='.';
    content=$(
      echo Blog:
      echo ''
      for i in $(ls txt | grep ^blog_ | sort -r)
      do
      	descr=$(head -n 1 txt/$i);
        echo "<a href="/html/$(rmtail $i).html">$descr</a>";
      done;
    );
  else
    dir='html';
    if [ $(echo $1 | cut -d '_' -f 1) = 'blog' ]
    then
      title=$(head -n 1 txt/$1 | sed 's/&/\\&/g');
    else
      title=$(rmtail $1);
    fi
    content=$(cat txt/$1);
  fi;
  compiled_content=$(
    echo "$template" | sed -n '2,/\.\./p' | sed "s/<title>/<title>$title/";
    echo "$nav";
    echo "$template" | sed -n '/youtube/,/<pre>/p';
    echo "$content"  | sed '/^http/s:.*:<a href="&" target="_blank">&</a>:';
    echo "$template" | sed -n '/<\/pre>/,$p';
  );
  file_content=$(cat "$dir/$(rmtail $1).html" 2>&1);
  if [ "$compiled_content" != "$file_content" ]
  then
    echo compile $(rmtail $1).html;
    echo "$compiled_content" > "$dir/$(rmtail $1).html";
  fi;
}

for i in $(echo index; ls txt)
do
  makehtml $i;
done;
