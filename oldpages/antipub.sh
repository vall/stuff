#!/bin/sh

if [ ! $EDITOR ]
then
  EDITOR=vi
fi

# Define actions

usage() {
  echo 'Usage: sh antipub.sh <command> [hash]'
  echo '  add      - add new post. The post hash generates automatically'
  echo '  rm  hash - remove post with given hash number'
  echo '  ed  hash - edit post with given hash number'
  echo '  re       - rebuild all html files'
  exit
}

cleanlines() {
  # remove two last empty lines
  sed '${/^ *$/d}' -i $1
  sed '${/^ *$/d}' -i $1
}

add() {
  tmpfile=$(mktemp -t antipub.XXXXXXXX)
  $EDITOR $tmpfile
  cleanlines $tmpfile
  if [ -s $tmpfile ]
  then
    newwall="$(cat $tmpfile wall.txt 2>/dev/null)"
    sha="$(echo "$newwall" | shasum -a 256 | cut -d ' ' -f 1 | grep -o '^......')"
    (echo '====='$sha'====='
    LANG=C date "+%d %b %Y"
    echo "$newwall") > wall.txt
  fi
}

remove() {
  continuefrom="$(sed -n "/^=====$1=====/,/^=====/p" wall.txt | tail -1)"
  newwall="$(sed -e "/^=====$1=====/,\$d" wall.txt
             sed -n -e "/$continuefrom/,\$p" wall.txt)"
  echo "$newwall" > wall.txt
}

edit() {
  echo edit $1;
}

rebuild() {
  (sed '/<\/main>/,$d' template.html
   cat wall.txt
   sed -n '/<\/main/,$p' template.html) > index.html
}

# Finally run action on post

if [ $# -gt 2 ]
then
  usage
fi

case $1 in
add) add;       rebuild ;;
rm)  remove $2; rebuild ;;
ed)  edit   $2; rebuild ;;
re)  rebuild   ;;
*)   usage     ;;
esac
