/* build this statically:						*/
/* cc -o qrsw qrsw.c -static -lX11 -lxcb -lpthread -lXau -lXdmcp	*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>

#define SCANDELAY 5
#define LFCODE    36
#define BARSTR    "00000123456789"
#define F5        71

static Display *dpy;
int skiperrs(Display *dpy, XErrorEvent *errevent);

int main()
{
	XEvent event, f5event;
	Window win;
	int ret, count = 0, scanner = 0;
	unsigned long prevtime = 0;
	char *qr;

	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "qrsw: error: can not open display\n");
		return -1;
	}

	XSetErrorHandler(&skiperrs);
	XGetInputFocus(dpy, &win, &ret);
	XSelectInput(dpy, win, FocusChangeMask | KeyPressMask);

	qr = malloc(64);
	qr[0] = '0';	/* first char never changes */
	for ( ; ; ) {
		XNextEvent(dpy, &event);
		if (event.type == FocusOut) {
			XGetInputFocus(dpy, &win, &ret);
			XSelectInput(dpy, win, FocusChangeMask | KeyPressMask);
		}
		else if (event.type == KeyPress) {
			if (event.xkey.time - prevtime < SCANDELAY) {
				if (count < 4)
					count++;
				if (event.xkey.keycode == LFCODE) {
					if (count >= 4 && !scanner) {
						XkbLockGroup(dpy, XkbUseCoreKbd, 0);
						scanner = 1;
					}
					if (scanner && !strcmp(qr, BARSTR)) {
						f5event = event;
						f5event.xkey.keycode = F5;
						XSendEvent(dpy, win, False, KeyPressMask, &f5event);
					}
					qr[1] = '\0';
				}
				else if (strlen(qr) < 64)
					strcat(qr, XKeysymToString(XKeycodeToKeysym(dpy, event.xkey.keycode, 0)));
			}
			else {
				if (count > 0)
					count--;
				else if (scanner)
					scanner = 0;
				qr[1] = '\0';
			}
			prevtime = event.xkey.time;
		}
	}

	XCloseDisplay(dpy);
	return 0;
}

int skiperrs(Display *dpy, XErrorEvent *errevent)
{
	return 0;
}
