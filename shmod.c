#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	int i, m, mask;
	struct stat sb;

	if (argc == 1) {
		fprintf(stderr, "shmod: usage: shmod file ...\n");
		return -1;
	}
	for (i = 1; i < argc; i++) {
		if (lstat(argv[i], &sb) == -1) {
			fprintf(stderr,"shmod: %s: %s\n", argv[i],
			    strerror(errno));
			return -1;
		}
		m = sb.st_mode;
		if (S_ISCHR(m))
			printf("character file");
		if (S_ISDIR(m))
			printf("directory");
		if (S_ISBLK(m))
			printf("block file");
		if (S_ISREG(m))
			printf("regular file");
		if (S_ISLNK(m))
			printf("symbolic link");
		printf("\t");
		for (mask = 0400; mask; mask = mask >> 1) {
			if (m & mask & 0444)
				printf("r");
			else if (m & mask & 0222)
				printf("w");
			else if (m & mask & 0111)
				printf("x");
			else
				printf("-");
		}
		printf("\t%o\t%s\n", m & 0777, argv[i]);
	}
	return 0;
}
