#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void
parse(char *str, int *from1, int *to1, char *cmd, int *from2, int *to2)
{
#define a2i(num) while (isdigit(*str)) num = 10 * num + *str++ - '0'
	
	*from1 = *from2 = *to1 = *to2 = 0;
	a2i(*from1);
	if (*str == ',') {
		str++;
		a2i(*to1);
	}
	else
		*to1 = *from1;
	*cmd = *str++;
	a2i(*from2);
	if (*str == ',') {
		str++;
		a2i(*to2);
	}
	else
		*to2 = *from2;
}

void nskip(FILE *file, int n)
{
	char buf[BUFSIZ];

	while (n--)
		fgets(buf, BUFSIZ, file);
}

void ncopy(FILE *file, int n, FILE *fout)
{
	char buf[BUFSIZ];

	while (n--) {
		fgets(buf, BUFSIZ, file);
		fputs(buf, fout);
	}
}

FILE * efopen(char *filename, char *mode)
{
	FILE *file;

	if ((file = fopen(filename, mode)) == NULL) {
		fprintf(stderr, "idiff: error: can't open file %s\n", filename);
		exit(1);
	}
	return file;
}

int main(int argc, char *argv[])
{
	FILE *diffout, *oldfile, *newfile, *ttyin, *fout, *vitemp;
	char buf[BUFSIZ], cmd, choice[3], *vitempname = "/tmp/idiff.XXXXX";
	int n, from1, to1, from2, to2, nf1, nf2;

	nf1 = nf2 = 0;
	if (argc != 3) {
		fprintf(stderr, "diff: usage: diff file1 file2\n");
		exit(1);
	}
	oldfile = efopen(argv[1], "r");
	newfile = efopen(argv[2], "r");
	ttyin = efopen("/dev/tty", "r");
	fout = efopen("idiff.out", "w");
	sprintf(buf, "diff %s %s", argv[1], argv[2]);
	diffout = popen(buf, "r");

	while (fgets(buf, BUFSIZ, diffout) != NULL) {
		parse(buf, &from1, &to1, &cmd, &from2, &to2);
		n = to1 - from1 + to2 - from2 + 1;
		if (cmd == 'c')
			n += 2;
		else if (cmd == 'a')
			from1++;
		else if (cmd == 'd')
			from2++;
		else {
			fprintf(stderr, "idiff: error: unknown diff command\n");
			exit(1);
		}
		printf("%s", buf);
		while (n--) {
			fgets(buf, BUFSIZ, diffout);
			printf("%s", buf);
		}
		do {
			printf("? ");
			fgets(choice, BUFSIZ, ttyin);
			switch(choice[0]) {
			case '>':
				nskip(oldfile, to1 - nf1);
				ncopy(newfile, to2 - nf2, fout);
				break;
			case '<':
				nskip(newfile, to2 - nf2);
				ncopy(oldfile, to1 - nf1, fout);
				break;
			case 'e':
				vitemp = efopen(mktemp(vitempname), "w");
				ncopy(oldfile, from1 - nf1 - 1, fout);
				nskip(newfile, from2 - nf2 - 1);
				ncopy(oldfile, to1 - from1 + 1, vitemp);
				fprintf(vitemp, "---\n");
				ncopy(newfile, to2 - from2 + 1, vitemp);
				fclose(vitemp);
				sprintf(buf, "vi %s", vitempname);
				system(buf);
				vitemp = efopen(vitempname, "r");
				while(fgets(buf, BUFSIZ, vitemp) != NULL)
					fputs(buf, fout);
				unlink(vitempname);
				break;
			case EOF:
				exit(0);
			default:
				printf("> or < or e\n");
				break;
			}
		} while (choice[0] != '>' && choice[0] != '<' &&
		    choice[0] != 'e');
		nf1 = to1;
		nf2 = to2;
	}
	while (fgets(buf, BUFSIZ, oldfile) != NULL)
		fputs(buf, fout);

	pclose(diffout);
	return 0;
}
